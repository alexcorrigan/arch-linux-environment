#!/bin/sh

echo "**** $0 - Linking directories in ~ ****"

function rm_if_exists {
	if [[ -L ~/$1 ]];
	then
		rm -r ~/$1
	fi
}

function ln_to_config {
	ln -s $(pwd)/$1 ~/$1
}

function echo_it {
	echo $1
}

DIRS=( launchers hows )

for DIR in ${DIRS[@]}
do
	echo_it $DIR
	rm_if_exists $DIR
	ln_to_config $DIR
done
