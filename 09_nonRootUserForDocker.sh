#!/bin/sh

echo "**** $0 - Add user to docker group so don't need too"

sudo groupadd docker
sudo usermod -aG docker $USER
