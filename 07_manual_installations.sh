#!/bin/sh

echo "**** $0 - Performing manual installations ****"

for INST in $(ls manual_installs/*.sh)
do
	echo "- $INST"
	./$INST
done
