#!/bin/bash

yay -S betterlockscreen

betterlockscreen -u ~/images/desktop/backgrounds/default_background

#enable lock screen when waking from suspend
sudo systemctl enable betterlockscreen@$USER
