#!/bin/bash

./01_build_filesystem.sh
./02_create_system_config_links.sh

source ~/.profile

./03_create_app_config_links.sh
./04_link_environment_dirs.sh
./05_install_packages.sh
./06a_install_yay.sh
./06b_install_aur_packages.sh
./07_manual_installations.sh
./08_links_to_share_drive.sh
./09_nonRootUserForDocker.sh
./10_rm_files_that_get_in_the_way.sh
./11_set_desktop_background.sh
