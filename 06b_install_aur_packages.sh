#!/bin/sh

echo "**** $0 - Installing AUR packages"

./aur_package_installs/install_better_lock_screen.sh
./aur_package_installs/install_polybar.sh
./aur_package_installs/install_urlview.sh
