#!/bin/bash

VERSION="v2.1.0"
ZIP_FILE="Hack.zip"

URL="https://github.com/ryanoasis/nerd-fonts/releases/download/${VERSION}/${ZIP_FILE}"

wget -O ~/tmp/$ZIP_FILE $URL

unzip -d ~/tmp ~/tmp/$ZIP_FILE

mv ~/tmp/Hack*Nerd*Complete.ttf ~/.fonts

fc-cache -fv

rm ~/tmp/$ZIP_FILE ~/tmp/*.ttf


