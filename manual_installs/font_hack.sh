#!/bin/bash

VERSION="v3.003"
ZIP_FILE="Hack-${VERSION}-ttf.zip"

URL="https://github.com/source-foundry/Hack/releases/download/${VERSION}/${ZIP_FILE}"

wget -O ~/tmp/$ZIP_FILE $URL

unzip -d ~/tmp ~/tmp/$ZIP_FILE

mv ~/tmp/ttf/*.ttf ~/.fonts

fc-cache -fv

rm -r ~/tmp/$ZIP_FILE ~/tmp/ttf


