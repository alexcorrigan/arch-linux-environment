#!/bin/bash

VERSION="3.2.0.8-1"
FILE="expressvpn-${VERSION}-x86_64.pkg.tar.xz"
URL="https://www.expressvpn.works/clients/linux/${FILE}"

wget -O ~/tmp/${FILE} ${URL}

tar -xvf ~/tmp/${FILE}

