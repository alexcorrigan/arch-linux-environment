#!/bin/bash

INSTALL_SCRIPT="tresorit_installer.run"
URL="https://installerstorage.blob.core.windows.net/public/install/$INSTALL_SCRIPT"

wget -O ~/tmp/$INSTALL_SCRIPT $URL

chmod +x ~/tmp/$INSTALL_SCRIPT

~/tmp/$INSTALL_SCRIPT

rm ~/tmp/$INSTALL_SCRIPT
