WORKSPACE_HOME="~/workspace"
WORK_WORKSPACE="$WORKSPACE_HOME/work"
PERSONAL_WORKSPACE="$WORKSPACE_HOME/personal"

alias wsp="cd $PERSONAL_WORKSPACE"
alias wsw="cd $WORK_WORKSPACE"

alias ls="ls --color -Aa"
alias ll="ls -aAlFh"

alias cat="bat --theme=gruvbox-dark"

alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="fgrep --color=auto"

alias vi="vim"
alias irc="$IRC_CLIENT"

alias t="task"
alias ti="capture_task.sh"
alias tnx="t +next"

alias dotfiles="cd ~/linux-environment/dotfiles"

#taskell
TODO_HOME="~/todo"
alias cpt="taskell $TODO_HOME/capture.md"

#source the 'how' aliases
source ~/hows/.how_aliases
