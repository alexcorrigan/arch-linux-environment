#!/bin/bash

# kill any existing instances
killall -q polybar

# wait for processes to shutdown
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# launch polybar
polybar top_bar &
#polybar bottom_bar &

echo "Polybar launched..."
