set nocompatible	" ignore any vi hangover
syntax enable		" detect and highlight syntax!!!
set path+=.**		" search recursively through every sub-dir, when finding a file

set expandtab
set tabstop=4		" interpret TAB as 4 spaces
set softtabstop=4	" write 4 spaces when editing for each TAB
set shiftwidth=4
set autoindent
set smartindent
set paste
set hidden			" allow opening buffers even current buffer not written

set encoding=utf-8
set fileformat=unix

" line numbers
set number          " show line numbers, current line number (within file)
set relativenumber  " line numbers are relative to line cursor is on

" toggle line numbers
nmap <F5> :silent set number! relativenumber! showmode! showcmd! hidden! ruler!<CR>

" presentation mode
noremap <Left> :silent bp<CR> :redraw!<CR> 
noremap <Right> :silent bn<CR> :redraw!<CR> 

" formatting
nmap <leader>T :.!figlet -f small<CR> " Turn text under cursor to big ascii art text
nmap <leader>* :normal i★<CR>		  " Print a unicode start

set showcmd		    " show last executed command in bottom right
filetype plugin indent on	"filetype detection on, use specific filetype indentation rules and plugins
set wildmenu		" visual autocomplete on command menu
set showmatch		" highlight matching parenthesies
set incsearch       " search as characters typed
set hlsearch        " highlight search matches
set backspace=2
set backspace=indent,eol,start
set cursorline      " highlight current line

set formatoptions-=cro      " disable continuing comments on newline

set noswapfile
set scrolloff=7

vnoremap < <gv
vnoremap > >gv

" appearance

"let g:gruvbox_termcolors = 256
"let g:gruvbox_contrast_dark = 'hard'
"let g:gruvbox_improved_string = 1
"colorscheme gruvbox
"set background=dark

colorscheme srcery
set background=dark
