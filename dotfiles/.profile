# This is sourced in login shells only. Not if a terminal is opened in a window manager after user already logged in. 

#export XDG_DATA_HOME="$HOME/.local.share"
#export XDG_CACHE_HOME="$HOME/.cache"
#export XDG_CONFIG_HOME="$HOME/.config"

export TERMINAL='xterm -ls'
export EDITOR='vim'
export BROWSER='firefox'

##
# paths
##

export SHARE_DRIVE="$HOME/TresoritDrive/share"

export WORK_CONFIG="${HOME}/work_config"

export WORKSPACE="~/workspace"
export PERSONAL_WORKSPACE="$WORKSPACE/personal"
export WORK_WORKSPACE="$WORKSPACE/work"

export SCRIPTS=$HOME/shell-scripts
DEV_TOOLS=$HOME/devTools

JAVA_HOME=/usr/lib/jvm/jdk-11.0.7
GRADLE_HOME=$DEV_TOOLS/gradle
MVN_HOME=$DEV_TOOLS/maven
SPARK_HOME=/opt/spark/spark-3.2.1-bin-hadoop3.2

export TEMPLATES="${HOME}/templates"

#jetbrains apps
JETBRAINS_HOME=/opt/jetbrains
IDEA_HOME=$JETBRAINS_HOME/idea
PYCHARM_HOME=$JETBRAINS_HOME/pycharm
DATAGRIP_HOME=$JETBRAINS_HOME/datagrip


PATH="$PATH:$SCRIPTS:$IDEA_HOME/bin:$PYCHARM_HOME/bin:$DATAGRIP_HOME/bin:$DEV_TOOLS:$JAVA_HOME/bin:$GRADLE_HOME/bin:$MVN_HOME/bin:$SPARK_HOME/bin:$SPARK_HOME/sbin"


##
# dmenu
##

    # font
    DMENU_FONT_SIZE=12
    export DMENU_FONT="Hack-${DMENU_FONT_SIZE}"


    #colours
    DMENU_DARK_SHADE="#1d2021"
    DMENU_LIGHT_SHADE="#ebdbb2"
    export DMENU_NORMAL_FG=$DMENU_LIGHT_SHADE
    export DMENU_NORMAL_BG=$DMENU_DARK_SHADE
    export DMENU_SELECTED_FG=$DMENU_DARK_SHADE
    export DMENU_SELECTED_BG=$DMENU_LIGHT_SHADE


##
# colours
##
export FG_DEFAULT='\e[39m'
export BG_DEFAULT='\e[49m'

export FG_BLACK='\e[30m'
export FG_RED='\e[31m'
export FG_GREEN='\e[32m'
export FG_YELLOW='\e[33m'
export FG_BLUE='\e[34m'
export FG_PURPLE='\e[35m'
export FG_CYAN='\e[36m'
export FG_LIGHTGRAY='\e[37m'

export BG_BLACK='\e[40m'
export BG_RED='\e[41m'
export BG_GREEN='\e[42m'
export BG_YELLOW='\e[43m'
export BG_BLUE='\e[44m'
export BG_PURPLE='\e[45m'
export BG_CYAN='\e[46m'
export BG_LIGHTGRAY='\e[47m'

export FG_DARKGRAY='\e[90m'
export FG_LRED='\e[91m'
export FG_LGREEN='\e[92m'
export FG_LYELLOW='\e[93m'
export FG_LBLUE='\e[94m'
export FG_LPURPLE='\e[95m'
export FG_LCYAN='\e[96m'
export FG_WHITE='\e[97m'

export BG_DARKGRAY='\e[100m'
export BG_LRED='\e[101m'
export BG_LGREEN='\e[102m'
export BG_LYELLOW='\e[103m'
export BG_LBLUE='\e[104m'
export BG_LPURPLE='\e[105m'
export BG_LCYAN='\e[106m'
export BG_WHITE='\e[107m'

##
# text style
# detailed here: https://askubuntu.com/a/985386
##
export ST_NORMAL='\033[0m'
export ST_BOLD='\033[1m'
export ST_DIM='\033[2m'
export ST_ITALIC='\033[3m'
export ST_UNDERLINE='\033[4m'
export ST_DOUBLE_UNDERLINE='\e[21m'
export ST_CURLY_UNDERLINE='\e[4:3m'
export ST_BLINK='\e[5m'
export ST_REVERSE='\e[7m'
export ST_HIDDEN='\e[8m'
export ST_STRIKETHROUGH='\e[9m'
export ST_OVERLINE='\e[53m'

###
# Hex Colours
###
export HEX_BLACK="#1d2021"
export HEX_GRAY="#928374"

export HEX_RED="#cc241d"
export HEX_BRIGHT_RED="#fb4934"

export HEX_ORANGE="#d65d0e"
export HEX_BRIGHT_ORANGE="#fe8019"

#################
# Gum properties
#################
export GUM_HI_LIGHT=$HEX_ORANGE

# input
export GUM_INPUT_PROMPT_FOREGROUND=$GUM_HI_LIGHT

# confirm
export GUM_CONFIRM_PROMPT_FOREGROUND=$HEX_BRIGHT_RED
export GUM_CONFIRM_SELECTED_FOREGROUND=$HEX_BLACK
export GUM_CONFIRM_SELECTED_BACKGROUND=$GUM_HI_LIGHT
export GUM_CONFIRM_UNSELECTED_FOREGROUND=$HEX_BLACK
export GUM_CONFIRM_UNSELECTED_BACKGROUND=$HEX_GRAY

# spinner
export GUM_SPIN_SPINNER_FOREGROUND=$GUM_HI_LIGHT


###
# source private profiles
###
PRIVATE_PROFILE_FILE='.profile_private'
if [ -f ~/$PRIVATE_PROFILE_FILE ]; then
	source ~/$PRIVATE_PROFILE_FILE
fi

###
# source host specific profiles
###
HOST_PROFILE_FILE=".profile_${HOSTNAME}"
if [ -f ~/$HOST_PROFILE_FILE ]; then
	source ~/$HOST_PROFILE_FILE
fi

#######################################################################################################################################
#
# Below here are sourcing bashrc and paths. Leave this section at bottom so that any variables set above can be used in the below
#
#######################################################################################################################################

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

######################################################################################################################################
