##
# source aliases
##
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


##
# prompt
##
PROMPT_COMMAND=_prompt_command

CMD_SUCCESS="✔"
CMD_FAIL="✘"

function _prompt_command {
    local PREV_EXIT_CODE=$?
    local PROMPT_ENDING="> "

    PS1=''
    PS2=''

	PS1+="\n"

    # set the colour based on previous exit code
    if [ $PREV_EXIT_CODE != 0 ]; then
        PS1+="${FG_RED}${CMD_FAIL}"
    else
        PS1+="${FG_GREEN}${CMD_SUCCESS}"
    fi

	PS1+=" ${FG_YELLOW}$(date +%H:%M:%S)${FG_LIGHTGRAY}$(date_based_prompt_message)${FG_DEFAULT}"
	#PS1+=" ${FG_LCYAN}\u@\h${FG_DEFAULT}:" # user@host

    WORKSPACE_PATH=$(workspace_path.sh)

    if [ ! -z $WORKSPACE_PATH ]
    then
        # if this is a workspace directory
        PS1+=" ${FG_LBLUE}WS: ${WORKSPACE_PATH}${FG_DEFAULT}"
    else
        # if this is NOT a workspace directory
		if [ $(pwd) == ~ ]
		then
			PROMPT_LOCATION="~ "
		else
			PROMPT_LOCATION="\w"
		fi
		PS1+=" ${FG_LBLUE}${PROMPT_LOCATION}${PROMPT_PATH}${FG_DEFAULT}"
    fi

	DEV_INFO=''

	# append current git branch if in a git project directory
	if git rev-parse --git-dir > /dev/null 2>&1; then
		#DEV_INFO="${DEV_INFO}${FG_LYELLOW}[GIT ${FG_DARKGRAY}$(prompt_git_summary.sh)${FG_LYELLOW}]${FG_DEFAULT}"
		DEV_INFO="${DEV_INFO}$(prompt_git_summary.sh)${FG_DEFAULT}"
	fi

    # append python virtual environment if activated
    if [ ! -z ${VIRTUAL_ENV} ]; then
		DEV_INFO="${DEV_INFO}$(prompt_python_venv_summary.sh)"
    fi

	if [[ ! -z $DEV_INFO ]]; then
		PS1+="\n"
		PS1+="  $DEV_INFO"
	fi

    PS1+="\n"

    PS1+="$PROMPT_ENDING"
    PS2+="$PROMPT_ENDING"
}
