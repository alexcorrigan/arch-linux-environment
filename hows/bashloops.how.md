# bash Loops

## for

### value in sequence
```
for i in {2..5}; do echo $i; done
```

### files in ls query
```
for i in *.ext; do echo $i; done
```
### values in array
```
ARRAY=("abc", "mno", "xyz")

for VALUE in "${ARRAY[@]}"
do
	echo $VALUE
done
```

## while

### read each line in a file

```
while read l; do echo $l; done < file.txt
```

### infinite loop
```
while true
do
	echo "hello"
done
