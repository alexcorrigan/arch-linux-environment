# VIM Folding Commands

`zo` -- opens a fold underneath the cursor.
`zO` -- opens all folds underneath the cursor, recursively.

`zc` -- closes a fold underneath the cursor.
`zC` -- closes all folds underneath the cursor, recursively.

`za` -- toggles a fold under the cursor (a closed fold is opened, an opened fold is closed).

`zR` -- opens all folds in the buffer.
`zM` -- closes all folds in the buffer.

`zr` -- opens a level of fold in the buffer.
`zm` -- closes a level of fold in the buffer.

Never use them, never remember them when I open someone else's file.
