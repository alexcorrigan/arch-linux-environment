# GPG / GPG2

- from <https://www.thepolyglotdeveloper.com/2018/12/manage-passwords-gpg-command-line-pass/>

## New Key
`gpg2 --full-generate-key`

Choose options:
- size = 4096
- expiry = something, if set to expire it can be renewed / new expiry set later

## List Keys
`gpg2 --list-keys`

To get the GPG id...
```
				pub   rsa4096 2018-11-30 [SC]
GPG id --->			  1A35565C9FE601446AC30BDE55238D2674A7BAD4
				uid           [ultimate] Test User <test@test.com>
				sub   rsa4096 2018-11-30 [E]
```
