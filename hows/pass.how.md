# pass

- from <https://www.thepolyglotdeveloper.com/2018/12/manage-passwords-gpg-command-line-pass/>

## Initialise New Password Store
`pass init <GPG key id>`

The gpg key id is from a key generated with `gpg2` (see howgpg).

The password store will be create at `~/.password-store/` and contain a file with the gpg id in it.

## List Passwords
`pass`

## Generate a New Password
`pass generate <where to store password eg /email/service/account> <num of characters>`

The password will be stored in the password store location here:

```
.password-store
|- email
  |- service
     |- account.gpg
```
pass will generate a random string of x characters with numbers and symbols.

## Add Existing Password
`pass insert <location to store>`

Will be prompted to write the existing password

## Copy Password to Clipboard
`pass -c <location of password>`

Password will be copied to clipboard

## Edit Pass / Add More Information
`pass edit <location of password>`

Will prompt for master password and open editor with the password.

Extra fields can be added to this file:
```
fwjkhfeuhiuofhew
username: abc@123.com
```

## Remove a Password
`pass remove <location of password>`

Will be prompted to confirm, then it will be gone.
