#!/bin/sh

echo "**** $0 - Install YAY"

pushd ~/tmp

git clone https://aur.archlinux.org/yay.git
pushd yay
makepkg -si --noconfirm
popd
rm -rf yay

popd
