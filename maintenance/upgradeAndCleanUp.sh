#!/bin/sh

echo "*** Delete any partially downloaded packages ***"
sudo find /var/cache/pacman/pkg/ -iname "*.part" -delete || exit 1

echo "*** Update developer keys and keyring ***"
sudo pacman -Sy archlinux-keyring || exit 1

echo "*** Do full update ***"
sudo pacman -Syu || exit 1

echo "*** Clean package cache ***"
sudo pacman -Sc
