#!/bin/sh

echo "**** $0 - Installing pacman packages ****"

install_package() {
	sudo pacman -S -q --noconfirm $1
}

while IFS= read -r PACKAGE
do
	echo "- $PACKAGE"
	install_package $PACKAGE
done < packages.txt

