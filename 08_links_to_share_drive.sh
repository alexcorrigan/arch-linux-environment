#!/bin/sh

echo "**** $0 - Links to share drive in ~"

function rm_if_exists {
	if [[ -L ~/$1 ]];
	then
		rm -r ~/$1
	fi
}

function link_to_share {
	ln -s $SHARE_DRIVE/$1 ~/$1
}

LINKS=( bookmarks.txt playlists todo )

for LINK in ${LINKS[@]};
do 
	echo $LINK
	rm_if_exists $LINK
	link_to_share $LINK
done
