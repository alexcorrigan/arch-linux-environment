```
   _          _      ___         _        _ _   ___ _               
  /_\  _ _ __| |_   |_ _|_ _  __| |_ __ _| | | / __| |_ ___ _ __ ___
 / _ \| '_/ _| ' \   | || ' \(_-<  _/ _` | | | \__ \  _/ -_) '_ (_-<
/_/ \_\_| \__|_||_| |___|_||_/__/\__\__,_|_|_| |___/\__\___| .__/__/
                                                           |_|      
Release 2021.06.01===================================================
```

Alex Corrigan
2020-10-25

Traget Machine:
- Packard Bell - imedia S2870
- Intel Pentium G2020
- Intel HD Graphics
- 500GB
- 4GB DDR3

Sources:
- Main arch installation guide: https://wiki.archlinux.org/title/Installation_guide
- This guy on youtube: https://www.youtube.com/watch?v=a00wbjy2vns

1. Prepare installation medium
- wrote the arch iso to USB flash drive following steps here: https://wiki.archlinux.org/index.php/USB_flash_installation_medium#Using_basic_command_line_utilities

2. Boot the target machine to the installation medium
- on boot loader menu, select "Arch Linux install medium" when prompted for boot option
- should land on the "root@archiso ~ #" prompt

3. Installation
- Set keyboard layout: `loadkeys uk`
- Verify boot mode: `ls /sys/firmware/efi/efivars` ... directory should list fine, this means boot mode is UEFI and can partition accordingly later
- (Plugged in external wifi adaptor as the aerials on the built in one are broker)
- Authenticate to wifi network:
    - `iwctl`
    - `[iwd]# device list` ... should show all available network adaptors
    - `[iwd]# station wlan1 scan` ... wlan1 was the new wifi adaptor, this should scan for available networks but not show until...
    - `[iwd]# station wlan1 get-networks` ... list networks found in the scan with signal strength (could help identify which is the right adaptor to use)
    - `[iwd]# station wlan1 connect <SSID>` ... will be prompted for passphrase to connect to the network
    - `[iwd]# station wlan1 show` ... should show the adaptor's connection status to the network: "State    Connected" is good!
    - `[iwd]# exit` ... quit iwctl util back to archiso prompt
    - `ping archlinux.org` ... should get some successful pings through the newly connected internet connection
- Set system clock and synch to ntp `timedatectl set-ntp true` (verify with `timedatectl status`)
- Partition disk
    -Identify system disk `fdisk -l` ... will be something like `sda`
        - `fdisk -l sda` will identify existing partitions on the device
    - `fdisk /dev/sda` will start fdisk against the identified device
        - `g` to create new GUID partition table (for UEFI boot mode)
        - `n` for new partition (boot partition):
            - Partition Number = 1
            - First Sector = 'accept default'
            - Last Sector = +500M
            (- YES to removing any existing signature)
        - `t` to declare type for that partition
            - Partition type = 1 (for EFI)
        - `n` for new partition (root):
            - Partition Number = 2
            - First Sector = 'accept default'
            - Last Sector = +30G
            (- YES to removing any existing signature)
        - `n` for new partition (home):
            - Partition Number = 3
            - First Sector = 'accept default'
            - Last Sector = 'accept default' to use remainder
            (- YES to removing any existing signature)
        - `w` write everything
- Format partitions
    - `mkfs.fat -F32 /dev/sda1` ... format the boot partition
    - `mkfs.ext4 /dev/sda2` ... format root partition
    - `mkfs.ext4 /dev/sda3` ... format home partition
- Mount partitions
    - `mount /dev/sda2 /mnt`
    - `mkdir /mnt/home` create home directory in the root partition
    - `mount /dev/sda3 /mnt/home` mount the home partition in the home directory just created
- Create fstab
    - `mkdir /mnt/etc`    
    - `genfstab -U -p /mnt >> /mnt/etc/fstab`
- Install essentials
    - `pacstrap /mnt base linux linux-firmware`
- Initialise system
    - `arch-chroot /mnt` root into new system
	- `pacman -S dhcpcd networkmanager` ... some networking things to make life easier after install, eg 'nmtui'
    - `pacman -S vim` ... a text editor is going to be needed later for `visudo` and only works with vim
    - `ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime` set local timezone
    - `hwclock --systohc`
    - locale
        - uncomment "en_GB.UTF-8 UTF-8" in `/etc/locale.gen` 
        - generate the locales with `locale-gen`
        - create `/etc/locale.conf`
        - set the LANG variabe in `/etc/locale.conf`: `LANG=en_GB.UTF-8`
        - create `/etc/vconsole.conf`
        - set the KEYMAP variable in `/etc/vconsole.conf`: `KEYMAP=uk`
    - network
        - create `/etc/hostname`
        - add hostname to `/etc/hostname` (orion)
        - edit `/etc/hosts`:
            127.0.0.1	localhost
            ::1		    localhost
            127.0.1.1	orion.localdomain orion
        - install networkmanager and tools
            - `pacman -S networkmanager wpa_supplicant wireless_tools netctl`
            - enable networkmanager service: `systemctl enable NetworkManager`
    - Install base-devel tools: `pacman -S base-devel` ... has tools like `which`, `sudo` etc.
    - Set root password: `passwd`
    - Create user and add to users group: `useradd -m -g users -G wheel alex`
    - Set password for user: `passwd alex`
    - enable sudo for users in wheel group - uncomment `%wheel ALL=(ALL) ALL` in `visudo`
- Boot loader
    - install grub and other boot tools: `pacman -S grub efibootmgr dosfstools os-prober mtools`
    - make boot directory: `mkdir /boot/EFI`
    - mount the boot partition to that directory: `mount /dev/sda1 /boot/EFI`
    - install the grub boot loader to system: `grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=grub_uefi --recheck`
    - `cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo`
    - generate grub configuration: `grub-mkconfig -o /boot/grub/grub.cfg`
- Swap file (instead of swap partition as easier to resize)
	- create the swap file at 8G (twice the machine RAM): `dd if=/dev/zero of=/swapfile bs=1024 count=8192000 status=progress`
    - set permissions on the swapfile: `chmod 600 /swapfile`
    - format it as a swapfile: `mkswap /swapfile`
	- activate it: `swapon /swapfile`
    - backup `fstab`: `cp /etc/fstab /etc/fstab.bk`
	- edit `/etc/fstab` to add entry for the swapfile: `/swapfile none swap defaults 0 0`
- Other packages for this machine:
    - `intel-ucode`
    - As this machine as an intel graphics card the `mesa` pacakage is required (it might get installed as a dependency of xorg-server): `pacman -S mesa`
- Set locale permenantly for keyboard layout
	- `sudo localectl set-x11-keymap gb`
	- check the following file has been created: `/etc/X11/xorg.conf.d/00-keyboard.conf`
	- ... and contents of that file contains `"XkbLayout" "gb"`
- Finalise and quit
    - exit from chroot environment: `exit`
    - unmount all partitions: `umount -R /mnt`
    - `poweroff` to shutdown
    - remove installation medium
    - try and boot it!!!

4. First boot
- Log in with user
- run `nmtui` to activate a wifi network connection
    - run `ping` against a website after activated to check dns and route is resolved
- check if `man` is installed (it wasn't on this first boot), if not, install it: `sudo pacman -S man`
- install some packages so we can get the linux_environment project: `sudo pacman -S git`
- git clone linux-environment repo: `git clone https://gitlab.com/alexcorrigan/linux-environment.git`
- cd into the linux-environment directory and run `./build_environment.sh`
- source the profile: `source ~/.profile`
- install any other optional stuff, aur packages are not installed as part of the `build_environment` script
- reboot machine and log in
- `startx` should now start the window manager

