#!/bin/sh

echo "**** $0 - Building filesystem ****"

function mkdir_if_not_exists {
	if [ ! -d ~/$1 ];
	then
		echo "- ~/$1"
		mkdir -p ~/$1
	fi
}

DIRS=( downloads .config tmp devtools workspace/personal workspace/work .fonts repos .ssh )

for DIR in ${DIRS[@]}
do
	mkdir_if_not_exists $DIR
done
