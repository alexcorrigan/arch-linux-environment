#!/bin/sh

echo "**** $0 - Linking system config files in ~ ****"

DOTFILES_DIR="$(pwd)/dotfiles"

function rm_if_exists {
	if [[ -f ~/"$1" ]];
	then
		rm ~/"$1"
	fi
}

function ln_to_config {
	ln -s $DOTFILES_DIR/$1 ~/"$1"
}

function echo_it {
	echo $1
}

SYSTEM_CONFIGS=( .bashrc .bash_aliases .profile .xinitrc .Xdefaults .inputrc )

for SYSTEM_CONFIG in "${SYSTEM_CONFIGS[@]}"
do
	echo_it $SYSTEM_CONFIG
	rm_if_exists $SYSTEM_CONFIG
	ln_to_config $SYSTEM_CONFIG
done

ln -s $DOTFILES_DIR/.config/user-dirs.dirs ~/.config/.
