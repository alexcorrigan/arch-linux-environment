#!/bin/sh

echo "**** $0 - Linking app specific configs in ~/.config ****"

DOTFILES_DIR="$(pwd)/dotfiles"

function rm_if_exists {
	if [[ -d ~/.config/$1 ]];
	then
		rm -r ~/.config/$1
	fi
}

function ln_to_config {
	ln -s $DOTFILES_DIR/.config/$1 ~/.config/$1
}

function echo_it {
	echo $1
}

for APP_CONFIG in $(ls -d $DOTFILES_DIR/.config/* | xargs -n 1 basename) 
do
	echo_it $APP_CONFIG
	rm_if_exists $APP_CONFIG
	ln_to_config $APP_CONFIG
done

# vim special child
ln -s $DOTFILES_DIR/.vimrc ~/.vimrc
ln -s $DOTFILES_DIR/.vim ~/.vim/

